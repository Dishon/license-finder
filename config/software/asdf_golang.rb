# frozen_string_literal: true

name "asdf_golang"
default_version "1.15.5"

source url: "https://golang.org/dl/go#{version}.linux-amd64.tar.gz"
relative_path "go"

version "1.14.6" do
  source sha256: "5c566ddc2e0bcfc25c26a5dc44a440fcc0177f7350c1f01952b34d5989a0d287"
end
version "1.14.7" do
  source sha256: "4a7fa60f323ee1416a4b1425aefc37ea359e9d64df19c326a58953a97ad41ea5"
end
version "1.15" do
  source sha256: "2d75848ac606061efe52a8068d0e647b35ce487a15bb52272c427df485193602"
end
version "1.15.1" do
  source sha256: "70ac0dbf60a8ee9236f337ed0daa7a4c3b98f6186d4497826f68e97c0c0413f6"
end
version "1.15.2" do
  source sha256: "b49fda1ca29a1946d6bb2a5a6982cf07ccd2aba849289508ee0f9918f6bb4552"
end
version "1.15.5" do
  source sha256: "9a58494e8da722c3aef248c9227b0e9c528c7318309827780f16220998180a0d"
end

build do
  mkdir install_dir
  sync "#{project_dir}/", install_dir
end
