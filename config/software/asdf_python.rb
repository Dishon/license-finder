# frozen_string_literal: true

name "asdf_python"
default_version "3.8.5"

dependency "bzip2"
dependency "libffi"
dependency "liblzma"
dependency "libsqlite3"
dependency "libuuid"
dependency "libyaml"
dependency "ncurses"
dependency "openssl"
dependency "zlib"

source url: "https://python.org/ftp/python/#{version}/Python-#{version}.tar.xz"
relative_path "Python-#{version}"

version("3.8.5") { source md5: "35b5a3d0254c1c59be9736373d429db7" }
version("3.8.4") { source md5: "e16df33cd7b58702e57e137f8f5d13e7" }
version("3.8.3") { source md5: "3000cf50aaa413052aef82fd2122ca78" }
version("3.8.2") { source md5: "e9d6ebc92183a177b8e8a58cad5b8d67" }
version("3.8.1") { source md5: "b3fb85fd479c0bf950c626ef80cacb57" }
version("3.8.0") { source md5: "dbac8df9d8b9edc678d0f4cacdb7dbb0" }
version("3.7.7") { source md5: "172c650156f7bea68ce31b2fd01fa766" }
version("3.7.6") { source md5: "c08fbee72ad5c2c95b0f4e44bf6fd72c" }
version("3.7.5") { source md5: "08ed8030b1183107c48f2092e79a87e2" }
version("3.7.4") { source md5: "d33e4aae66097051c2eca45ee3604803" }
version("3.7.3") { source md5: "93df27aec0cd18d6d42173e601ffbbfd" }
version("3.7.2") { source md5: "df6ec36011808205beda239c72f947cb" }
version("3.7.1") { source md5: "0a57e9022c07fad3dadb2eef58568edb" }
version("3.7.0") { source md5: "eb8c2a6b1447d50813c02714af4681f3" }
version("3.6.11") { source md5: "8f91c770b546a4938efbdb3064796c6c" }
version("3.5.9") { source md5: "ef7f82485e83c7f8f8bcb920a9c2457b" }
version("3.4.10") { source md5: "f88a98bce17a03c43a6a5f8a66ab2e62" }
version("3.3.7") { source md5: "84e2f12f044ca53b577f6224c53f82ac" }
version("3.2.6") { source md5: "e0ba4360dfcb4aec735e666cc0ae7b0e" }
version("3.1.5") { source md5: "20dd2b7f801dc97db948dd168df4dd52" }
version("3.0.1") { source md5: "7291eac6a9a7a3642e309c78b8d744e5" }

version("2.7.18") { source md5: "fd6cc8ec0a78c44036f825e739f36e5a" }

build do
  env = with_standard_compiler_flags(with_embedded_path)
  configure_command = [
    "--enable-shared",
    "--enable-ipv6",
    "--prefix=#{install_dir}/embedded",
    "--with-ssl=#{install_dir}/embedded",
    "--with-dbmliborder=",
    "--without-dtrace",
    "--without-valgrind"
  ]
  configure(*configure_command, env: env)
  make "-j #{workers}", env: env
  make "-j #{workers} install", env: env
  command "ln -s #{install_dir}/embedded/bin #{install_dir}/bin", env: env

  if version.start_with?('3.')
    command "ln -s #{install_dir}/bin/python3 #{install_dir}/bin/python", env: env
    command "ln -s #{install_dir}/bin/pip3 #{install_dir}/bin/pip", env: env
  end

  case version
  when /^3\.3\./
    command "curl https://bootstrap.pypa.io/3.3/get-pip.py -o #{project_dir}/get-pip.py", env: env
    command "#{install_dir}/bin/python #{project_dir}/get-pip.py \"pip==10.0.1\"", env: env
    command "#{install_dir}/bin/pip install pipenv==2018.6.25 virtualenv==16.0.0", env: env
  when /^3\.4\./
    command "#{install_dir}/bin/pip install pipenv==11.1.6 virtualenv==20.0.33", env: env
  when /^2\.7\./
    command "curl https://bootstrap.pypa.io/2.6/get-pip.py -o #{project_dir}/get-pip.py", env: env
    command "#{install_dir}/bin/python #{project_dir}/get-pip.py \"pip<20.0\"", env: env
    command "#{install_dir}/bin/pip install pipenv==2020.8.13 virtualenv==20.0.33", env: env
  else
    command "#{install_dir}/bin/pip install pipenv==2020.8.13 virtualenv==20.0.33", env: env
  end

  if version == default_version
    command "#{install_dir}/bin/pip install conan", env: env
    command "#{install_dir}/bin/conan --version", env: env
  end
  command "#{install_dir}/bin/pip --version", env: env
  command "#{install_dir}/bin/pipenv --version", env: env
  command "#{install_dir}/bin/virtualenv --version", env: env
end

build do
  command "find #{install_dir} -type f -name '*.exe' -exec rm {} +"
  command "find #{install_dir} -type d -name '__pycache__' -exec rm -r {} +"
  command "find #{install_dir} -type d -name 'test' -exec rm -r {} +"
  command "find #{install_dir} -type d -name 'tests' -exec rm -r {} +"
  delete "#{install_dir}/embedded/bin/sqlite3"
  delete "#{install_dir}/embedded/lib/python*/unittest"
  delete "#{install_dir}/embedded/lib/python*/lib-dynload/_bsddb*"
  delete "#{install_dir}/embedded/lib/python*/lib-dynload/readline*"
  delete "#{install_dir}/embedded/man"
  delete "#{install_dir}/embedded/share"
end
