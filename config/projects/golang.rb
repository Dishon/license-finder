# frozen_string_literal: true

golang_version = ENV.fetch('GOLANG_VERSION', '1.15.5')

name "golang-#{golang_version}"
maintainer "GitLab B.V."
homepage "https://golang.org/"

install_dir "/opt/asdf/installs/golang/#{golang_version}/go"
package_scripts_path Pathname.pwd.join("config/scripts/golang")

build_version golang_version
build_iteration 1

override 'asdf_golang', version: golang_version
dependency "asdf_golang"

package :deb do
  compression_level 9
  compression_type :xz
end
