# frozen_string_literal: true

module LicenseFinder
  class NPM
    def possible_package_paths
      [project_path.join('package.json')]
    end

    def prepare
      within_project_path do
        tool_box.install(tool: :nodejs, env: default_env)

        if lockfile?
          shell.execute([:npm, :ci, "--production"], env: default_env, capture: false)
        else
          shell.execute([:npm, :install, '--no-save', "--production"], env: default_env, capture: false)
        end
      end
    end

    def current_packages
      NpmPackage.packages_from_json(npm_json, detected_package_path).map do |item|
        Dependency.from(item, detected_package_path)
      end
    end

    private

    def lockfile?
      File.exist?(project_path.join('package-lock.json'))
    end

    def npm_json
      stdout, _stderr, status = within_project_path do
        shell.execute([:npm, "list", "--json", "--long", "--production"])
      end
      status.success? ? JSON.parse(stdout) : {}
    end

    def default_env
      {
        'NPM_CONFIG_CAFILE' => ENV.fetch('NPM_CONFIG_CAFILE', shell.default_certificate_path).to_s
      }
    end
  end
end
