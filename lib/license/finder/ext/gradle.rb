# frozen_string_literal: true

module LicenseFinder
  class Gradle
    def prepare
      within_project_path do
        tool_box.install(tool: :java, version: java_version, env: default_env)
      end
    end

    def current_packages
      return [] unless download_licenses

      Pathname
        .glob(project_path.join('**', 'dependency-license.xml'))
        .map(&:read)
        .flat_map { |xml_file| parse_from(xml_file) }.uniq
    end

    def package_management_command
      wrapper? ? project_path.join('gradlew') : :gradle
    end

    private

    def java_version(env: ENV)
      @java_version ||= tool_box.version_of(:java, env: env)
    end

    def download_licenses
      _stdout, _stderr, status = within_project_path do
        shell.execute([
          @command,
          ENV.fetch('GRADLE_CLI_OPTS', '--exclude-task=test --no-daemon --debug'),
          'downloadLicenses'
        ], env: default_env)
      end

      status.success?
    end

    def wrapper?
      project_path.join('gradlew').exist?
    end

    def xml_parsing_options
      @xml_parsing_options ||= { 'GroupTags' => { 'dependencies' => 'dependency' } }
    end

    def parse_from(xml_file)
      XmlSimple
        .xml_in(xml_file, xml_parsing_options)
        .fetch('dependency', []).map { |hash| map_from(hash) }
    end

    def map_from(hash)
      Dependency.from(GradlePackage.new(hash, include_groups: @include_groups), detected_package_path)
    end

    def default_env
      @default_env = {
        'ASDF_JAVA_VERSION' => ENV.fetch('ASDF_JAVA_VERSION', java_version),
        'CACHE_DIR' => '/opt/gitlab',
        'JAVA_HOME' => ENV.fetch("JAVA_HOME", "/opt/asdf/installs/java/#{java_version}"),
        'TERM' => 'noop'
      }
    end
  end
end
