plugins {
  `java-library`
}

repositories {
  jcenter()
}

dependencies {
  runtime("org.postgresql:postgresql:42.1.4")
  implementation("com.google.guava:guava:28.1-jre")
  testImplementation("junit:junit:4.12")
}
