# frozen_string_literal: true

require 'spec_helper'

RSpec.describe LicenseFinder::Yarn do
  let(:package_manager) { described_class.new(options) }
  let(:options) { { ignored_groups: [], project_path: project.project_path } }
  let(:project) { ProjectHelper.new }

  before do
    project.mount(dir: project_fixture)
  end

  after do
    project.cleanup
  end

  describe "#nodejs_version" do
    subject { package_manager.send(:nodejs_version) }

    context "when the version of nodejs is specified in a .tool-versions file" do
      let(:project_fixture) { fixture_file('js/10.21.0-tool-versions') }

      specify { expect(subject).to eql('10.21.0') }
    end

    context "when a nodejs version is not specified" do
      let(:project_fixture) { fixture_file('js/yarn/single-declared-dependency') }

      specify { expect(subject).to eql('12.18.2') }
    end
  end
end
