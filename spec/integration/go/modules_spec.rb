# frozen_string_literal: true

require 'spec_helper'

RSpec.describe "modules" do
  subject { runner.scan(env: env) }

  let(:env) { {} }

  before do
    # Delete go module cache
    system('rm -fr /opt/gitlab/.local/pkg')
    system('rm -fr /opt/asdf/installs/golang/**/packages/pkg/')
  end

  include_examples "each report version", "go", "modules", "66bc04cf"

  ['1.11', '1.12', '1.13', '1.14', '1.15', '1.15.1'].each do |version|
    context "when scanning a go.mod and go.sum files with v#{version}" do
      before do
        runner.add_file('main.go', fixture_file_content('go/main.go'))
        runner.add_file('go.mod', fixture_file_content('go/go.mod'))
        runner.add_file('go.sum', fixture_file_content('go/go.sum'))
        runner.add_file('.tool-versions', "golang #{version}")
      end

      specify do
        expect(subject).to match_schema
        expect(subject[:licenses]).not_to be_empty
        expect(subject.dependency_names).to match_array([
          "cloud.google.com/go",
          "github.com/davecgh/go-spew",
          "github.com/dimfeld/httptreemux/v5",
          "github.com/go-logfmt/logfmt",
          "github.com/golang/protobuf",
          "github.com/google/uuid",
          "github.com/pmezard/go-difflib",
          "github.com/stretchr/objx",
          "github.com/stretchr/testify",
          "golang.org/x/net",
          "golang.org/x/oauth2",
          "golang.org/x/sync",
          "golang.org/x/text",
          "google.golang.org/appengine",
          "gopkg.in/check.v1",
          "gopkg.in/yaml.v2"
        ])
        expect(subject.licenses_for('github.com/dimfeld/httptreemux/v5')).to match_array(['MIT'])
        expect(subject.licenses_for('github.com/go-logfmt/logfmt')).to match_array(['MIT'])
        expect(subject.licenses_for('github.com/google/uuid')).to match_array(['BSD-3-Clause'])
        expect(subject.licenses_for('github.com/stretchr/testify')).to match_array(['MIT'])
        expect(subject.licenses_for('golang.org/x/oauth2')).to match_array(['BSD-3-Clause'])
      end
    end
  end

  context "when scanning the `gitaly` project" do
    before do
      runner.clone('https://gitlab.com/gitlab-org/gitaly.git')
    end

    specify do
      expect(subject).to match_schema
      expect(subject[:licenses]).not_to be_empty
      expect(subject[:dependencies]).not_to be_empty
    end

    specify { expect { subject }.to perform_under(60).sec.warmup(0).times }
  end

  context "when scanning the `gitlab-runner` project" do
    before do
      runner.clone('https://gitlab.com/gitlab-org/gitlab-runner.git')
    end

    specify do
      expect(subject).to match_schema
      expect(subject[:licenses]).not_to be_empty
      expect(subject[:dependencies]).not_to be_empty
      expect(subject.dependency_names).to include('gitlab.com/gitlab-org/gitlab-terminal')
    end
  end

  context "when scanning a project with vendored modules" do
    before do
      runner.mount(dir: fixture_file('go/1.14-vendored-modules'))
    end

    specify do
      expect(subject).to match_schema
      expect(subject.dependency_names).to match_array([
        "github.com/davecgh/go-spew",
        "github.com/konsorten/go-windows-terminal-sequences",
        "github.com/pmezard/go-difflib",
        "github.com/sirupsen/logrus",
        "github.com/stretchr/testify",
        "golang.org/x/sys"
      ])
      expect(subject.licenses_for("github.com/davecgh/go-spew")).to match_array(['unknown'])
      expect(subject.licenses_for("github.com/konsorten/go-windows-terminal-sequences")).to match_array(['MIT'])
      expect(subject.licenses_for("github.com/pmezard/go-difflib")).to match_array(['unknown'])
      expect(subject.licenses_for("github.com/sirupsen/logrus")).to match_array(['MIT'])
      expect(subject.licenses_for("github.com/stretchr/testify")).to match_array(['unknown'])
      expect(subject.licenses_for("golang.org/x/sys")).to match_array(['BSD-3-Clause'])
    end
  end

  context "when scanning a project sourced from a TLS endpoint with a X.509 certificate signed by a private authority" do
    before do
      runner.mount(dir: fixture_file('go/1.14-ignore-tls'))
    end

    context "when the CA certificate is provided" do
      let(:env) do
        {
          'ADDITIONAL_CA_CERT_BUNDLE' => x509_certificate.read,
          'GOPROXY' => 'https://goproxy.test'
        }
      end

      specify do
        expect(subject).to match_schema
        expect(subject.dependency_names).to match_array([
          "github.com/davecgh/go-spew",
          "github.com/google/go-cmp",
          "github.com/google/licenseclassifier",
          "github.com/pmezard/go-difflib",
          "github.com/sergi/go-diff",
          "github.com/stretchr/objx",
          "github.com/stretchr/testify"
        ])
      end
    end

    context "when the CA certificate is NOT provided" do
      let(:env) { { 'GOPROXY' => 'https://goproxy.test' } }

      specify { expect(subject).to match_schema }
    end
  end

  context "when scanning a go.mod file located in a sub directory" do
    let(:env) { { 'LICENSE_FINDER_CLI_OPTS' => '--recursive' } }

    before do
      runner.mount(dir: fixture_file('go/1.15-subdir'))
    end

    it 'produces the proper report' do
      expect(subject).to match_schema
      expect(subject.dependency_names).to match_array([
        "github.com/BurntSushi/toml",
        "github.com/cpuguy83/go-md2man/v2",
        "github.com/davecgh/go-spew",
        "github.com/google/renameio",
        "github.com/julienschmidt/httprouter",
        "github.com/kisielk/gotool",
        "github.com/kr/pretty",
        "github.com/kr/pty",
        "github.com/kr/text",
        "github.com/pkg/errors",
        "github.com/pmezard/go-difflib",
        "github.com/rogpeppe/go-internal",
        "github.com/russross/blackfriday/v2",
        "github.com/shurcooL/sanitized_anchor_name",
        "github.com/stretchr/objx",
        "github.com/stretchr/testify",
        "github.com/urfave/cli",
        "go.uber.org/atomic",
        "go.uber.org/multierr",
        "go.uber.org/tools",
        "go.uber.org/zap",
        "golang.org/x/crypto",
        "golang.org/x/lint",
        "golang.org/x/mod",
        "golang.org/x/net",
        "golang.org/x/sync",
        "golang.org/x/sys",
        "golang.org/x/text",
        "golang.org/x/tools",
        "golang.org/x/xerrors",
        "gopkg.in/check.v1",
        "gopkg.in/errgo.v2",
        "gopkg.in/yaml.v2",
        "honnef.co/go/tools"
      ])
    end
  end
end
