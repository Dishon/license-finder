# frozen_string_literal: true

require 'spec_helper'

RSpec.describe "maven" do
  subject { runner.scan(env: env) }

  let(:env) { {} }

  include_examples "each report version", "java", "maven", "b29564b2"
  include_examples "each report version", "java", "maven-multimodules", "95b49a9a"

  context "when the maven dependencies come from the same projects public maven repository" do
    let(:env) { { 'CI_PROJECT_ID' => '18446184' } }

    before do
      runner.mount(dir: fixture_file('java/maven/gitlab-repo'))
    end

    it 'is able to detect some of the licenses' do
      expect(subject).to match_schema
      expect(subject.dependency_names).to match_array(%w[example jaxb-api])
      expect(subject.licenses_for('example')).to match_array(['MIT'])
      expect(subject.licenses_for('jaxb-api')).to match_array(['GPL-2.0-only', 'CDDL-1.1'])
    end
  end

  context "when packages are sourced from an external package registry" do
    let(:env) do
      {
        'CI_PROJECT_ID' => 'invalid',
        'MAVEN_CLI_OPTS' => "--settings settings.xml"
      }
    end

    before do
      runner.mount(dir: fixture_file('java/maven/external-gitlab-repo'))
    end

    it 'downloads packages from by using a custom `settings.xml`' do
      expect(subject).to match_schema
      expect(subject.dependency_names).to match_array(%w[example jaxb-api])
      expect(subject.licenses_for('example')).to match_array(['MIT'])
      expect(subject.licenses_for('jaxb-api')).to match_array(['GPL-2.0-only', 'CDDL-1.1'])
    end
  end

  describe "When using the `SETUP_CMD`" do
    let(:env) { { 'SETUP_CMD' => 'bash custom.sh' } }

    before do
      runner.add_file('custom.sh') do
        <<~SCRIPT
        #!/bin/bash -l

        echo 'hello'
        SCRIPT
      end
    end

    specify { expect(subject).to match_schema }
  end

  describe "When scanning a project with multiple modules" do
    before do
      runner.mount(dir: fixture_file('java/maven/multimodule'))
    end

    it 'detects dependences from each module' do
      expect(subject).to match_schema
      expect(subject[:dependencies]).not_to be_empty

      [
        { name: "asm", licenses: ["bsd"] },
        { name: "asm-commons", licenses: ["bsd"] },
        { name: "jackson-annotations", licenses: ["Apache-2.0"] },
        { name: "jackson-core", licenses: ["Apache-2.0"] },
        { name: "jackson-databind", licenses: ["Apache-2.0"] },
        { name: "jackson-dataformat-xml", licenses: ["Apache-2.0"] },
        { name: "jackson-module-jaxb-annotations", licenses: ["Apache-2.0"] },
        { name: "log4j-api", licenses: ["Apache-2.0"] },
        { name: "log4j-core", licenses: ["Apache-2.0"] },
        { name: "netty-all", licenses: ["Apache-2.0"] },
        { name: "stax2-api", licenses: ["bsd"] }
      ].each do |dependency|
        expect(subject.licenses_for(dependency[:name])).to match_array(dependency[:licenses])
      end

      expect(subject.dependency_names).not_to include('junit')
    end
  end

  [8, 11].each do |java_version|
    context "when connecting to a custom package registry with a self signed certificate using JAVA #{java_version}" do
      let(:env) { { 'ADDITIONAL_CA_CERT_BUNDLE' => x509_certificate.read, 'LM_JAVA_VERSION' => java_version.to_s } }

      before do
        runner.add_file('pom.xml') do
          fixture_file_content('java/maven/pom-single.xml.erb', {
            group_id: 'com.fasterxml.jackson.core',
            artifact_id: 'jackson-core',
            version: '2.10.0',
            repository_id: 'custom',
            repository_url: "https://maven.test/maven2"
          })
        end
      end

      specify do
        expect(subject).to match_schema
        expect(subject.dependency_names).to match_array(['jackson-core'])
        expect(subject.licenses_for('jackson-core')).to match_array(['Apache-2.0'])
      end
    end
  end

  [
    { java: '8', maven: ['3.6.3', '3.5.4', '3.3.9', '3.2.5'] },
    { java: '11', maven: ['3.6.3', '3.5.4', '3.3.9', '3.2.5'] }
  ].each do |item|
    item[:maven].each do |maven_version|
      context "when using Java v#{item[:java]} with maven v#{maven_version}" do
        let(:env) do
          {
            'LM_JAVA_VERSION' => item[:java],
            'MAVEN_CLI_OPTS' => "--settings settings.xml"
          }
        end

        before do
          runner.mount(dir: fixture_file('java/maven/simple'))
          runner.add_file('.mvn/wrapper/maven-wrapper.properties') do
            "distributionUrl=https://repo1.maven.org/maven2/org/apache/maven/apache-maven/#{maven_version}/apache-maven-#{maven_version}-bin.zip"
          end
        end

        specify do
          expect(subject).to match_schema
          expect(subject.dependency_names).to match_array(['netty-all'])
          expect(subject.licenses_for('netty-all')).to match_array(['Apache-2.0'])
        end
      end
    end
  end

  context "when scanning maven project in a sub directory" do
    let(:env) do
      {
        'LICENSE_FINDER_CLI_OPTS' => '--project-path=service/maven-backend'
      }
    end

    before do
      runner.mount(dir: fixture_file('java/maven/nested'))
    end

    specify { expect(subject).to match_schema }

    specify do
      expect(subject.dependency_names).to match_array([
        "accessors-smart",
        "android-json",
        "annotations",
        "apiguardian-api",
        "asm",
        "assertj-core",
        "byte-buddy",
        "byte-buddy-agent",
        "hamcrest",
        "jakarta.activation-api",
        "jakarta.annotation-api",
        "jakarta.xml.bind-api",
        "json-path",
        "json-smart",
        "jsonassert",
        "jul-to-slf4j",
        "junit-jupiter",
        "junit-jupiter-api",
        "junit-jupiter-engine",
        "junit-jupiter-params",
        "junit-platform-commons",
        "junit-platform-engine",
        "kotlin-reflect",
        "kotlin-stdlib",
        "kotlin-stdlib-common",
        "kotlin-stdlib-jdk7",
        "kotlin-stdlib-jdk8",
        "log4j-api",
        "log4j-to-slf4j",
        "logback-classic",
        "logback-core",
        "mockito-core",
        "mockito-junit-jupiter",
        "objenesis",
        "opentest4j",
        "slf4j-api",
        "snakeyaml",
        "spring-aop",
        "spring-beans",
        "spring-boot",
        "spring-boot-autoconfigure",
        "spring-boot-starter",
        "spring-boot-starter-logging",
        "spring-boot-starter-test",
        "spring-boot-test",
        "spring-boot-test-autoconfigure",
        "spring-context",
        "spring-core",
        "spring-expression",
        "spring-jcl",
        "spring-test",
        "xmlunit-core"
      ])
    end
  end
end
