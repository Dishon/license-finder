# frozen_string_literal: true

require 'spec_helper'

RSpec.describe "bundler" do
  subject { runner.scan(env: env) }

  let(:env) { {} }

  before do
    system('rm -fr /opt/asdf/installs/ruby')
  end

  include_examples "each report version", "ruby", "bundler", "8194b658"

  context "when the project depends on an older version of ruby specified in a `.ruby-version` file" do
    before do
      runner.mount(dir: fixture_file('ruby/bundler-ruby-2.4.9-no-lockfile'))
    end

    it 'installs the required ruby and produces a valid report' do
      expect(subject).to match_schema
      expect(subject[:licenses]).not_to be_empty
      expect(subject[:dependencies].map { |x| x[:name] }).to include("saml-kit")
    end
  end

  context "when a project depends on an older version of bundler" do
    before do
      runner.mount(dir: fixture_file('ruby/bundler-v1.17'))
    end

    it 'produces a valid report' do
      expect(subject).to match_schema
      expect(subject[:licenses]).not_to be_empty
      expect(subject.dependency_names).to include("saml-kit")
    end
  end

  context "when a project depends on bundler `~> 2.1`" do
    before do
      runner.mount(dir: fixture_file('ruby/bundler-v2.1'))
    end

    it 'produces a valid report' do
      expect(subject).to match_schema
      expect(subject[:licenses]).not_to be_empty
      expect(subject.dependency_names).to include('net-hippie')
      expect(subject.licenses_for('net-hippie')).to match_array(['MIT'])
    end
  end

  context "when passing custom options to license finder" do
    let(:env) do
      {
        'LICENSE_FINDER_CLI_OPTS' => "--debug --aggregate-paths=. ruby"
      }
    end

    before do
      runner.clone('https://gitlab.com/gitlab-org/gitaly.git', branch: 'v12.10.13')
    end

    it 'forwards the options to license finder' do
      expect(subject).to match_schema
      expect(subject.dependency_names).to include('rbtrace')
    end
  end

  context "when parsing a project with versions of gems that conflict with license_finder" do
    before do
      runner.mount(dir: fixture_file('ruby/bundler-gem-conflict'))
    end

    specify do
      expect(subject).to match_schema
      expect(subject[:licenses]).not_to be_empty
      expect(subject[:dependencies]).not_to be_empty
      expect(subject.dependency_names).to include('rails')
    end
  end

  context "when scanning the `gitlab` project" do
    before do
      runner.clone('https://gitlab.com/gitlab-org/gitlab.git')
      system("rm #{runner.project_path}/config/dependency_decisions.yml")
    end

    specify do
      expect(subject).to match_schema
      expect(subject.dependency_names).to include('rails')
      expect(subject.dependency_names).to include('vue')
    end
  end

  context "when fetching dependencies from a custom registry" do
    before do
      runner.mount(dir: fixture_file('ruby/bundler-custom-tls'))
    end

    context "when the CA certificate is provided" do
      let(:env) do
        {
          'ADDITIONAL_CA_CERT_BUNDLE' => x509_certificate.read,
          'LOG_LEVEL' => 'debug'
        }
      end

      # This test is flaky. The reverse proxy to rubygems.org periodically fails.
      xspecify do
        expect(subject).to match_schema
        expect(subject.dependency_names).to include("net-hippie")
        expect(subject.licenses_for('net-hippie')).to match_array('MIT')
      end
    end

    context "when the CA certificate is NOT provided" do
      let(:env) { {} }

      specify { expect(subject).to match_schema }
    end
  end

  context "when fetching dependencies from a git repo" do
    before do
      runner.mount(dir: fixture_file('ruby/bundler-git-source'))
    end

    specify do
      expect(subject).to match_schema
      expect(subject.dependency_names).to include("net-hippie")
      expect(subject.licenses_for('net-hippie')).to match_array(['MIT'])
    end
  end

  context "when scanning a projects with a gems.lock" do
    before do
      runner.mount(dir: fixture_file('ruby/bundler-v2.1-gems.lock'))
    end

    specify do
      expect(subject).to match_schema
      expect(subject[:licenses]).not_to be_empty
      expect(subject.dependency_names).to include('spandx')
      expect(subject.licenses_for('spandx')).to match_array(['MIT'])
    end
  end

  context "when scanning a jruby project" do
    before do
      runner.mount(dir: fixture_file('ruby/bundler-jruby'))
    end

    pending do
      expect(subject).to match_schema
      expect(subject[:licenses]).not_to be_empty
      expect(subject.dependency_names).not_to include('jruby-profiler-flame_graph_profile_printer')

      expect(subject.dependency_names).to include('activerecord-jdbcpostgresql-adapter')
      expect(subject.dependency_names).to include('protobuf_java_helpers')
      expect(subject.dependency_names).to include('fast_blank_java')

      expect(subject.licenses_for('activerecord-jdbcpostgresql-adapter')).to match_array(['MIT'])
      expect(subject.licenses_for('protobuf_java_helpers')).to match_array(['MIT'])
      expect(subject.licenses_for('fast_blank_java')).to match_array(['MIT'])
    end
  end
end
